package de.bitkorn.dom.svg;

import org.apache.batik.dom.svg.AbstractSVGItem;

/**
 *
 * @author Torsten Brieskorn
 */
public class SVGItem extends AbstractSVGItem {

    @Override
    protected String getStringValue() {
        return itemStringValue;
    }
    
}
