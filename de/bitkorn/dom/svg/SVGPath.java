package de.bitkorn.dom.svg;

import java.util.List;
import org.apache.batik.dom.svg.AbstractSVGPathSegList;
import org.apache.batik.dom.svg.SVGPathSegItem;
import org.w3c.dom.DOMException;
import org.w3c.dom.svg.SVGException;

/**
 *
 * @author Torsten Brieskorn
 */
public class SVGPath extends AbstractSVGPathSegList {

    public String getPathAsString() {
        SVGPathSegItem itemTmp;
        for (Object item : itemList) {
            if (item instanceof SVGPathSegItem) {
                itemTmp = (SVGPathSegItem) item;
                System.out.println("item.getClass(): " + itemTmp.getClass());
                System.out.println("item.toString(): " + itemTmp.toString());
            }
        }

        return "";
    }

    /**
     *
     * @return
     */
    @Override
    protected String getValueAsString() throws ClassCastException {
        return "";
    }

    @Override
    protected SVGException createSVGException(short s, String string, Object[] os) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void setAttributeValue(String string) {
        System.out.println("protected void setAttributeValue(String string)");
    }

    @Override
    protected DOMException createDOMException(short s, String string, Object[] os) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getNumberOfItems() {
        return this.itemList.size();
    }
}
