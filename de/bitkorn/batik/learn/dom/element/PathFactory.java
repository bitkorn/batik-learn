package de.bitkorn.batik.learn.dom.element;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * X and Y are at the top left.
 * Path information at https://www.w3.org/TR/SVG/paths.html
 * 
 * @author Torsten Brieskorn
 */
public class PathFactory {

    private final Document doc;
    private final Element path;
    private String attributeDvalueM = "";
    private String attributeD = "";

    public PathFactory(Document doc) {
        this.doc = doc;
        this.path = this.doc.createElement("path");
        this.path.setAttribute("style", "fill:none; stroke:black; stroke-width:2");
    }
    
    public void setStartPoint(float mx, float my){
        this.attributeDvalueM = "M" + mx + "," + my;
    }
    
    public void lineTo(float x, float y){
        this.attributeD += " L" + x + "," + y;
    }
    
    /**
     * Draws a cubic Bézier curve from the current point to (x,y) using (x1,y1) as the control point at the beginning of the curve and (x2,y2) as the control point at the end of the curve
     * @param x1 control point at the beginning of the curve
     * @param y1 control point at the beginning of the curve
     * @param x2 control point at the end of the curve
     * @param y2 control point at the end of the curve
     * @param x point to
     * @param y point to
     */
    public void curveTo(float x1, float y1, float x2, float y2, float x, float y){
        this.attributeD += " C" + x1 + "," + y1 + " " + x2 + "," + y2 + " " + x + "," + y;
    }

    public Element generatePath(){
        this.path.setAttribute("d", this.attributeDvalueM + this.attributeD);
        return this.path;
    }
}
