/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.bitkorn.batik.learn.dom;

import de.bitkorn.batik.learn.dom.element.PathFactory;
import java.io.File;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.batik.anim.dom.SVGDOMImplementation;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Torsten Brieskorn
 */
public class BezierCurve {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        DOMImplementation impl = SVGDOMImplementation.getDOMImplementation();
        String svgNS = SVGDOMImplementation.SVG_NAMESPACE_URI;
        Document doc = impl.createDocument(svgNS, "svg", null);

        Element svgRoot = doc.getDocumentElement();
        svgRoot.setAttributeNS(null, "width", "200");
        svgRoot.setAttributeNS(null, "height", "200");

        PathFactory pf = new PathFactory(doc);
        pf.setStartPoint(10, 10);
        pf.lineTo(14, 16);
        pf.curveTo(70, 20, 70, 100, 120, 130);
        pf.curveTo(150, 150, 120, 200, 190, 160);

        svgRoot.appendChild(pf.generatePath());

        Source source = new DOMSource(doc);
        Result result = new StreamResult(new File("/home/allapow/tmp/out.svg"));

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, result);
//        org.apache.xmlgraphics
//org.w3c.dom.svg.SVGPathSeg;
    }
}
