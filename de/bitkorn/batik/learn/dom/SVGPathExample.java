
package de.bitkorn.batik.learn.dom;

import de.bitkorn.dom.svg.SVGPath;
import java.io.File;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.batik.anim.dom.SVGDOMImplementation;
import org.apache.batik.dom.GenericDOMImplementation;
import org.apache.batik.dom.GenericDocument;
import org.apache.batik.dom.GenericDocumentType;
import org.apache.batik.dom.svg.SVGPathSegItem;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;

/**
 *
 * @author Torsten Brieskorn
 */
public class SVGPathExample {

    /**
     * @param args the command line arguments
     * @throws java.lang.Exception
     */
    public static void main(String[] args) throws Exception {
//        DOMImplementation impl = SVGDOMImplementation.getDOMImplementation();
        GenericDOMImplementation impl = new GenericDOMImplementation();
        GenericDocumentType dt = new GenericDocumentType("My Doc", "my-id", "my-sys-id");
        GenericDocument doc = new GenericDocument(dt, impl);
        
        Element svgRoot = doc.getDocumentElement();
        svgRoot.setAttributeNS(null, "width", "400mm");
        svgRoot.setAttributeNS(null, "height", "450mm");
        
        SVGPathSegItem svgps = new SVGPathSegItem(SVGPathSegItem.PATHSEG_CURVETO_CUBIC_ABS, "MyLetter");
        svgps.setX(80);
        svgps.setY(90);
        svgps.setX1(40);
        svgps.setY1(60);
        svgps.setX2(110);
        svgps.setY2(100);
        System.out.println("svgps: " + svgps.getValueAsString());
        
        SVGPath svgp = new SVGPath();
        svgp.initialize(svgps);
        
        SVGPathSegItem svgps2 = new SVGPathSegItem(SVGPathSegItem.PATHSEG_CURVETO_CUBIC_ABS, "MyLetter");
        svgps2.setX(80);
        svgps2.setY(90);
        svgps2.setX1(40);
        svgps2.setY1(60);
        svgps2.setX2(110);
        svgps2.setY2(100);
        svgp.appendItem(svgps2);
        
        /*
        java.lang.ClassCastException: org.apache.batik.dom.svg.SVGPathSegItem cannot be cast to org.w3c.dom.svg.SVGPathSegCurvetoCubicAbs
        */
        System.out.println("svgp String: " + svgp.getPathAsString());
        
        /**
         * make file
         */
        Source source = new DOMSource(doc);
        Result result = new StreamResult(new File("/home/allapow/tmp/outPath.svg"));

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, result);
    }

}
